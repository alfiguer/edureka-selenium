package co.edureka.learning.devops.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class WebDriverTest {

	public void execute() {
		System.setProperty("webdriver.chrome.driver",
				"/Volumes/AFG-DATA/OCL-DATA/courses/DevOps-Certification-Training/src/selenium-drivers/chromedriver");

		WebDriver driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();

		System.out.println("Hi, Welcome to Edureka's YouTube Live session on Selenium WebDriver");

		driver.get("https://www.oracle.com/index.html");
		driver.manage().timeouts().implicitlyWait(3L, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(3L, TimeUnit.SECONDS);
		{
			WebElement element = driver.findElement(By.cssSelector(".u02user:nth-child(1)"));
			Actions builder = new Actions(driver);
			builder.moveToElement(element).perform();
		}
		{
			WebElement element = driver.findElement(By.tagName("body"));
			Actions builder = new Actions(driver);
			builder.moveToElement(element, 0, 0).perform();
		}
		driver.findElement(By.id("txtSearch")).click();
		driver.findElement(By.id("txtSearch")).sendKeys("Java");
		driver.findElement(By.cssSelector(".u02searchbttn")).click();
	}

	public static void main(String[] args) {
		WebDriverTest webDriverTest = new WebDriverTest();
		webDriverTest.execute();
	}

}
